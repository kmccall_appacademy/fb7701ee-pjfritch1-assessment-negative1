# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

def missing_numbers(nums)
  missing_nums = []
  nums[0...-1].each_with_index do |num, i|
    missing = num + 1
    while missing < nums[i + 1]
      missing_nums << missing
      missing += 1
    end
  end

  missing_nums
end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.

def base2to10(binary)
  base_10_sum = 0
  idx = binary.to_s.length - 1
  digits = binary.to_s.chars
  digits.map! {|dig| dig.to_i}
  digits.each do |dig|
    base_10_sum += dig * 2**idx
    idx -= 1
  end

  base_10_sum
end

class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

  def my_select(&prc)
    new_hash = {}
    self.each {|key,value| new_hash[key]=value if prc.call(key,value)}

    new_hash
  end

end

class Hash

  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.

  def my_merge(hash, &prc)
    new_hash = {}
    unless block_given?
      self.each do |key, value|
        if hash.has_key?(key)
          new_hash[key] = hash[key]
        else
          new_hash[key] = value
        end
      end
      hash.each_key {|key| new_hash[key] = hash[key] unless self.has_key?(key)}
      return new_hash
    end


    hash.each do |key, value|
      if self.has_key?(key)
        new_hash[key] = prc.call(key,self[key],value)
      else
        new_hash[key] = hash[key]
      end
    end

    self.each do |key, value|
      unless hash.has_key?(key)
        new_hash[key] = self[key]
      end
    end
    new_hash
  end

end

# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

def lucas_numbers(n)
  if n < 0
    negative_nums(n)
  else
    positive_nums(n)
  end
end

def positive_nums(n)
  lucas_nums = [2,1]
  (n-1).times do
    lucas_nums << (lucas_nums[-1] + lucas_nums[-2])
  end

  lucas_nums[n]
end

def negative_nums(n)
  lucas_nums = [-1,2]
  (n.abs).times do
    lucas_nums.unshift(lucas_nums[1] - lucas_nums[0])
  end

  lucas_nums[n-1]
end

# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false

def longest_palindrome(string)
  palindromes = finds_palindromes(string)
  palindromes.sort_by! {|word| word.length}

  if palindromes[-1].length > 2 && !palindromes.empty?
    palindromes[-1].length
  else
    false
  end
end

def finds_palindromes(string)
  palindromes = []
  chrs = string.chars
  pal_index = chrs.length - 1
  chrs.each_with_index do |chr, i|
    while pal_index > i + 1 && !is_palindrome?(chrs[i..pal_index])
      pal_index -= 1
    end
    palindromes << chrs[i..pal_index] if is_palindrome?(chrs[i..pal_index])

    pal_index = chrs.length - 1
  end

  palindromes
end

def is_palindrome?(arr)
  arr == arr.reverse
end
